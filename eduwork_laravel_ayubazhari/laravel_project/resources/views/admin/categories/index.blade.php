@extends('layouts.admin')
@section('title', 'Category')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>@yield('title')</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Home</a></li>
              <li class="breadcrumb-item active">@yield('title')</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List of Category</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('success') }}
                </div>
                @endif
                <a href="{{ route('categories.create') }}" class="btn btn-info rounded mb-3">
                  <i class="fas fa-plus"></i> Insert
                </a>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @php
                    $no = 1 + (($categories->currentPage() - 1) * $categories->perPage());
                  @endphp
                  @foreach ($categories as $category)
                  <tr>
                    <td>{{ $no++; }}</td>
                    <td>{{ $category->name }}</td>
                    <td>
                        <form onsubmit="return confirm('Apakah Anda Yakin Akan Menghapus?');" action="{{ route('categories.destroy', $category->id) }}" method="POST">
                            <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-warning text-white">
                                <i class="fas fa-edit"></i>
                                Edit
                            </a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">
                                <i class="fas fa-trash"></i>
                                Delete
                            </button>
                        </form>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
                <br>
                {{ $categories->links() }}
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
