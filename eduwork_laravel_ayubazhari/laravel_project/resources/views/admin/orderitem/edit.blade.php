@extends('layouts.admin')
@section('title', 'Order Item')

@section('content')
  <div class="content-wrapper">
   <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>@yield('title')</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Home</a></li>
              <li class="breadcrumb-item active">@yield('title')</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Edit Order Item</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <form action="{{url('admin/order/'.$orderitem->order_id.'/orderitem/'.$orderitem->id)}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <input type="hidden" name="order_id" value="{{$orderitem->order_id}}" id="order_id">
                        <div class="col-6">
                            <label for="product_id">Product Name</label>
                            <select class="form-control @error('product_id') is-invalid @enderror" name="product_id" id="product_id">
                                <option value="">--Pilih Product--</option>
                                @foreach ($products as $product)
                                <option value="{{ $product->id }}" {{ (old('product_id', $orderitem->product->name) == $product->name) ? 'selected' : '' }}>
                                    {{ $product->name }}
                                </option>
                                @endforeach
                            </select>
                            @error('product_id')
                            <div class="alert alert-danger mt-2">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="col-6">
                            <label for="qty">Quantity</label>
                            <input type="number" class="form-control @error('qty') is-invalid @enderror" name="qty" id="qty" value="{{old('qty', $orderitem->qty)}}">
                            @error('qty')
                            <div class="alert alert-danger mt-2">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <button type="submit" class="btn btn-info"><i class="fas fa-edit"></i> Update</button>
                    <button type="reset" class="btn btn-dark"><i class="fa fa-undo"></i> Cancel</button>
                </form>
              </div>
              <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

  </div>
@endsection
