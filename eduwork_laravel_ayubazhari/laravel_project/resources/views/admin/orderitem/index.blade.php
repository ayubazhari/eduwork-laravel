@extends('layouts.admin')
@section('title', 'Order Item')

@section('content')
  <div class="content-wrapper">
   <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>@yield('title')</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Home</a></li>
              <li class="breadcrumb-item active">@yield('title')</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">

          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Insert Order Item</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('success') }}
                </div>
                @endif
                <form action="{{url('admin/order/'.$id.'/store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <input type="hidden" name="order_id" value="{{$id}}" id="order_id">
                        <div class="col-6">
                            <label for="product_id">Product Name</label>
                            <select class="form-control @error('product_id') is-invalid @enderror" name="product_id" id="product_id">
                                <option value="">--Pilih Product--</option>
                                @foreach ($products as $product)
                                <option value="{{$product->id}}" {{ (old('product') == $product->name) ? 'selected' : '' }}>{{$product->name}}</option>
                            @endforeach
                            </select>
                            @error('product_id')
                            <div class="alert alert-danger mt-2">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="col-6">
                            <label for="qty">Quantity</label>
                            <input type="number" class="form-control @error('qty') is-invalid @enderror" name="qty" id="qty" placeholder="Masukkan Product Quantity">
                            @error('qty')
                            <div class="alert alert-danger mt-2">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <button type="submit" class="btn btn-info"><i class="fas fa-save"></i> Save</button>
                    <button type="reset" class="btn btn-dark"><i class="fa fa-times"></i> Reset</button>
                </form>
              </div>
            </div>
            <!-- /.card -->

            <div class="card">
                <div class="card-header">
                  <h3 class="card-title">List of Order Item</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Product Name</th>
                            <th>Quantity</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $no = 1;
                        @endphp
                        @foreach ($orderitems as $orderitem)
                        <tr>
                            <td>{{ $no++; }}</td>
                            <td>{{ $orderitem->product->name }}</td>
                            <td>{{ $orderitem->qty }}</td>
                            <td>
                                <a href="{{ url('admin/order/'.$id.'/orderitem/'.$orderitem->id) }}" class="btn btn-warning text-white"><i class="fas fa-edit"></i> Edit</a>
                                @csrf
                                <a href="{{ url('admin/order/'.$id.'/orderitem/'.$orderitem->id.'/destroy') }}" class="btn btn-danger" onclick="return confirm('Apakah Anda Yakin Akan Menghapus?')"><i class="fas fa-trash"></i> Delete</a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->

        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection

