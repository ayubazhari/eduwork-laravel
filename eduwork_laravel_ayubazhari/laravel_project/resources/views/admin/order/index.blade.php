@extends('layouts.admin')
@section('title', 'Order')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>@yield('title')</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Home</a></li>
              <li class="breadcrumb-item active">@yield('title')</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List of Order</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('success') }}
                </div>
                @endif
                <a href="{{ route('order.create') }}" class="btn btn-info rounded mb-3">
                    <i class="fas fa-plus"></i> Insert
                </a>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Order Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($orders as $order)
                    <tr>
                        <td>{{ $no++; }}</td>
                        <td>{{ $order->name }}</td>
                        <td>{{ date('d-m-y', strtotime($order->tanggal_order)) }}</td>
                        <td>
                            <a href="{{ url('admin/order/'.$order->id) }}" class="btn btn-primary"><i class="fas fa-cart-plus"></i> Add Item</a>
                            <a href="{{ url('admin/order/edit/'.$order->id) }}" class="btn btn-warning text-white"><i class="fas fa-edit"></i> Edit</a>
                            <a href="{{ url('admin/order/destroy/'.$order->id) }}" class="btn btn-danger" onclick="return confirm('Apakah Anda Yakin Akan Menghapus?')"><i class="fas fa-trash"></i> Delete</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
