<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('authentication/img/apple-icon.png') }}">
    <link rel="icon" type="image/png" href="https://w7.pngwing.com/pngs/384/470/png-transparent-retail-computer-icons-e-commerce-sales-mega-offer-miscellaneous-service-logo.png">
    <title>
      @yield('title')
    </title>
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css') }}">
    <!-- Nucleo Icons -->
    <link href="{{ asset('authentication/css/nucleo-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('authentication/css/nucleo-svg.css') }}" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
    <!-- CSS Files -->
    <link id="pagestyle" href="{{ asset('authentication/css/material-dashboard.css') }}" rel="stylesheet" />
</head>
<body>

    <div class="container position-sticky z-index-sticky top-0">
        <div class="row">
          <div class="col-12">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg blur border-radius-xl top-0 z-index-3 shadow position-absolute my-3 py-2 start-0 end-0 mx-4">
              <div class="container-fluid ps-2 pe-0">
                <a class="navbar-brand font-weight-bolder ms-lg-0 ms-3 " href="../pages/dashboard.html">
                  LAPTOP STORE
                </a>
                <button class="navbar-toggler shadow-none ms-2" type="button" data-bs-toggle="collapse" data-bs-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon mt-2">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                  </span>
                </button>
                <div class="collapse navbar-collapse" id="navigation">
                  <ul class="navbar-nav mx-auto"></ul>
                  <ul class="navbar-nav d-lg-block d-none">
                    <li class="nav-item">
                      <a class="nav-link me-2" href="{{ route('login') }}">
                        <i class="fas fa-key opacity-6 text-dark me-1"></i>
                        Login
                      </a>
                    </li>
                  </ul>
                  <ul class="navbar-nav d-lg-block d-none">
                    <li class="nav-item">
                      <a class="nav-link me-2" href="{{ route('register') }}">
                        <i class="fas fa-user-circle opacity-6 text-dark me-1"></i>
                        Register
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
            <!-- End Navbar -->
          </div>
        </div>
    </div>
    <main class="main-content  mt-0">
        <div class="page-header min-vh-100">
            <div class="container">
                <div class="row">
                    <div class="col-6 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 start-0 text-center justify-content-center flex-column">
                        <div class="position-relative bg-gradient-primary h-100 m-3 px-7 border-radius-lg d-flex flex-column justify-content-center" style="background-image: url('https://ds393qgzrxwzn.cloudfront.net/resize/c500x500/cat1/img/images/0/ODuixFs9Kn.jpg'); background-size: cover;"></div>
                    </div>
                    <div class="col-xl-4 col-lg-5 col-md-7 d-flex flex-column ms-auto me-auto ms-lg-auto me-lg-5">
                        <div class="card card-plain">
                            <div class="card-header mb-o">
                                <h4 class="font-weight-bolder">Belanja Sekarang</h4>
                                <p class="mb-0">Masukkan email dan password kamu untuk daftar</p>
                            </div>
                            <div class="card-body mt-0">
                                <form role="form" action="{{ route('register') }}" method="post">
                                    @csrf
                                    <div class="input-group input-group-outline">
                                        <label class="form-label">Name</label>
                                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" required autocomplete="name">
                                    </div>
                                    @error('name')
                                    <small class="text-danger">
                                        {{ $message }}
                                    </small>
                                    @enderror
                                    <div class="input-group input-group-outline mt-3">
                                        <label class="form-label">Email</label>
                                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" required autocomplete="email">
                                    </div>
                                    @error('email')
                                    <small class="text-danger mt-0">
                                        {{ $message }}
                                    </small>
                                    @enderror
                                    <div class="input-group input-group-outline mt-3">
                                        <label class="form-label">Password</label>
                                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" required autocomplete="new-password">
                                    </div>
                                    @error('password')
                                    <small class="text-danger">
                                        {{ $message }}
                                    </small>
                                    @enderror
                                    <div class="input-group input-group-outline mt-3">
                                        <label class="form-label">Confirm Password</label>
                                        <input type="password" class="form-control @error('confirm_password') is-invalid @enderror" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                    @error('confirm_password')
                                    <small class="text-danger">
                                        {{ $message }}
                                    </small>
                                    @enderror
                                    <div class="text-center mt-3">
                                        <button type="submit" class="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0">Register</button>
                                    </div>
                                </form>
                            </div>
                            <div class="card-footer text-center pt-0 px-lg-2 px-1">
                                <p class="mb-2 text-sm mx-auto">
                                    Sudah punya akun?
                                    <a href="{{ route('login') }}" class="text-primary text-gradient font-weight-bold">Login</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!--   Core JS Files   -->
    <script src="{{ asset('authentication/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('authentication/js/core/bootstrap.min.js') }}"></script>
    <script src="{{ asset('authentication/js/plugins/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('authentication/js/plugins/smooth-scrollbar.min.js') }}"></script>
    <script>
        var win = navigator.platform.indexOf('Win') > -1;
        if (win && document.querySelector('#sidenav-scrollbar')) {
        var options = {
            damping: '0.5'
        }
        Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
        }
    </script>
    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="{{ asset('authentication/js/material-dashboard.min.js?v=3.0.0') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @if(session('error'))
        <script>
            swal("Mesagge", "{{ session('error') }}", 'error');
        </script>
    @endif
</body>
</html>
