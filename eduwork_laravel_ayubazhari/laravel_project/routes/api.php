<?php

use App\Http\Controllers\Api\OrderItemController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/order')->group(function()
{
    Route::get('/orderitem/{id}', [OrderItemController::class, 'index']);
    Route::post('/orderitem/{id}', [OrderItemController::class, 'store']);
    Route::get('/{order_id}/orderitem/edit/{id}', [OrderItemController::class, 'edit']);
    Route::post('/{order_id}/orderitem/update/{id}', [OrderItemController::class, 'update']);
    Route::delete('/{order_id}/orderitem/{id}', [OrderItemController::class, 'destroy']);
});
