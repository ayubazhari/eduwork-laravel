<?php

use App\Http\Controllers\Admin\CategoriesController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\OrderItemController;
use App\Http\Controllers\Admin\ProductsController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Frontend\CartController;
use App\Http\Controllers\Frontend\CategoryController;
use App\Http\Controllers\Frontend\CheckoutController;
use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function() {
    return view('pages.auth.login');
});

Route::get('/home', [HomeController::class, 'index']);
Route::get('/product', [ProductController::class, 'index']);
Route::get('/category', [CategoryController::class, 'index']);
Route::get('/cart', [CartController::class, 'index']);
Route::get('/checkout', [CheckoutController::class, 'index']);
Route::get('/contact', [ContactController::class, 'index']);

Route::group(['middleware' => ['auth', 'verified', 'IsAdmin']], function () {
    Route::prefix('/admin')->group(function () {
        Route::resource('/dashboard', DashboardController::class);
        Route::resource('/users', UsersController::class);
        Route::resource('/categories', CategoriesController::class);
        Route::resource('/products', ProductsController::class);
        Route::prefix('order')->group(function()
        {
            Route::get('/', [OrderController::class, 'index'])->name('order.index');
            Route::get('/create', [OrderController::class, 'create'])->name('order.create');
            Route::post('/store',[OrderController::class, 'store'])->name('order.store');
            Route::get('/edit/{id}',[OrderController::class, 'edit'])->name('order.edit');
            Route::post('/update/{id}',[OrderController::class, 'update'])->name('order.update');
            Route::get('/destroy/{id}', [OrderController::class, 'destroy'])->name('order.destroy');
            // OrderItemController
            Route::get('/{id}', [OrderItemController::class, 'index']);
            Route::get('/{order_id}/orderitem/{id}', [OrderItemController::class, 'edit']);
            Route::post('/{order_id}/orderitem/{id}', [OrderItemController::class, 'update']);
            Route::post('/{id}/store', [OrderItemController::class, 'store']);
            Route::get('/{order_id}/orderitem/{id}/destroy', [OrderItemController::class, 'destroy']);
        });
    });
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
