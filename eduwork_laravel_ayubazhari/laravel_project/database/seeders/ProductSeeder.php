<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create('id_ID');
        foreach(range(1, 100) as $index) {
            DB::table('products')->insert([
                'code' => $faker->randomNumber(5),
                'name' => $faker->word(2),
                'stock' => $faker->randomNumber(3),
                'varian' => $faker->word(1),
                'description' => $faker->sentence(3),
                'image' => $faker->imageUrl($width = 100, $height = 100),
                'category_id' => rand(1, 5),
            ]);
        }
    }
}
