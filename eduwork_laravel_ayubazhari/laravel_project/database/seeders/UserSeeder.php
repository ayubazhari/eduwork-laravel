<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create('id_ID');
        foreach(range(1, 100) as $index) {
            DB::table('users')->insert([
                'name' => $faker->firstName(),
                'email' => $faker->email(),
                'password' => $faker->password(),
            ]);
        }
    }
}
