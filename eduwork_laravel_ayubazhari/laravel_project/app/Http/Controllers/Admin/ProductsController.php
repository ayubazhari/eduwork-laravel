<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = Product::paginate(5);
        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'category_id' => 'required',
            'name' => 'required',
            'code' => 'required',
            'stock' => 'required',
            'varian' => 'required',
            'description' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png|max:3072'
        ]);
        $product = new Product();
        if($request->hasfile('image'))
        {
            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            $filename = time().'.'.$ext;
            $file->move('assets/uploads/products', $filename);
            $product->image = $filename;
        }
        $product->name = $request->input('name');
        $product->category_id = $request->input('category_id');
        $product->code = $request->input('code');
        $product->stock = $request->input('stock');
        $product->varian = $request->input('varian');
        $product->description = $request->input('description');
        $product->save();
        return redirect()->route('products.index')->with('success', 'Data berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $product = Product::findOrFail($id);
        $categories = Category::all();
        return view('admin.products.edit', compact(['product', 'categories']));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $product = Product::find($id);
        $request->validate([
            'category_id' => 'required',
            'name' => 'required',
            'code' => 'required',
            'stock' => 'required',
            'varian' => 'required',
            'description' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png|max:3072'
        ]);
        if($request->hasFile('image'))
        {
        $path = 'assets/uploads/products/'.$product->image;
        if(File::exists($path))
        {
            File::delete($path);
        }
        $file = $request->file('image');
        $ext = $file->getClientOriginalExtension();
        $filename = time().'.'.$ext;
        $file->move('assets/uploads/products', $filename);
        $product->image = $filename;
        }
        $product->name = $request->input('name');
        $product->category_id = $request->input('category_id');
        $product->code = $request->input('code');
        $product->stock = $request->input('stock');
        $product->varian = $request->input('varian');
        $product->description = $request->input('description');
        $product->update();
        return redirect()->route('products.index')->with('success', 'Data berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return redirect()->route('products.index')->with(['success' => 'Data berhasil dihapus!']);
    }
}
