<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\OrderItem;
use App\Models\product;

class OrderItemController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index($id) {
    	$data['orderitems'] = OrderItem::where('order_id', $id)->get();
    	$data['id'] = $id;
    	$data['products'] = Product::get();
        return view('admin/orderitem/index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request) {
    	$request->validate([
    		'order_id' => 'required',
    		'product_id' => 'required',
    		'qty' => 'required'
    	]);
        DB::table('orderitems')->insert([
            'order_id'=> $request->order_id,
            'product_id'=> $request->product_id,
            'qty'=> $request->qty
        ]);
    	return back()->with('success','Data berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }


    /**
    * Show the form for editing the specified resource.
    */
    public function edit($order_id, $id) {
		$data['products'] = Product::get();
		$data['orderitem'] = OrderItem::where('id', $id)->where('order_id', $order_id)->first();
		return view('admin/orderitem/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $order_id, $id) {
		$request->validate([
    		'order_id' => 'required',
    		'product_id' => 'required',
    		'qty' => 'required'
    	]);
        DB::table('orderitems')->where('id', $id)->update([
            'order_id'=> $request->order_id,
            'product_id'=> $request->product_id,
            'qty'=> $request->qty
        ]);
    	return redirect('admin/order/'.$order_id)->with('success', 'Data berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($order_id ,$id) {
		DB::table('orderitems')->where('id', $id)->where('order_id', $order_id)->delete();
        return back()->with('success','Data berhasil dihapus');
    }
}
