<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\order;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users = DB::table('users')->get();
    	$orders = DB::table('orders')
            ->select('orders.id', 'orders.user_id', 'users.name', 'orders.tanggal_order')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->orderBy('orders.id', 'desc')
            ->get();
        return view('admin/order/index', compact(['users', 'orders']));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $users = DB::table('users')->get();
        return view('admin/order/create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'tanggal_order' => 'required',
        ]);
        $user_id = $request->input('user_id');
        $tanggal_order = $request->input('tanggal_order');
        $order = new order;
        $order->user_id = $user_id;
        $order->tanggal_order = $tanggal_order;
        $order->save();
        $id = $order->id;
        return redirect('admin/order/'.$id)->with('success', 'Data berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
		$users = DB::table('users')->get();
        $order = DB::table('orders')->where('id', $id)->first();
		return view('admin/order/edit', compact(['users', 'order']));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
    	$request->validate([
            'user_id' => 'required',
            'tanggal_order' => 'required',
    	]);
    	$user_id = $request->input('user_id');
    	$tanggal_order = $request->input('tanggal_order');
        $order = Order::find($id);
    	$order->user_id = $user_id;
    	$order->tanggal_order = $tanggal_order;
    	$order->update();
    	return redirect('admin/order/')->with('success', 'Data berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        DB::table('orders')->where('id', $id)->delete();
        return redirect('admin/order')->with('success', 'Data berhasil dihapus!');
    }
}

