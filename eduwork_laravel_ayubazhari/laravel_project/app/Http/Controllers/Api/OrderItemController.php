<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\OrderItem;
use Illuminate\Http\Request;

class OrderItemController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = OrderItem::with('Product')->where('order_id', 'id')->get();
        return response()->json(['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
    		'order_id' => 'required',
    		'product_id' => 'required',
    		'qty' => 'required|integer'
    	]);
        $data = $request->all();
        OrderItem::create($data);
        $orderitem =OrderItem::where('order_id', $data['order_id'])->get();
        return response()->json(['data' => $orderitem]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($order_id, $id)
    {
        $data = OrderItem::where('order_id', $order_id)->where('id', $id)->get();
        return response()->json(['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $order_id, $id)
    {
        $request->validate([
    		'order_id' => 'required',
    		'product_id' => 'required',
    		'qty' => 'required|integer'
    	]);
        $result = OrderItem::where('order_id', $order_id)->where('id', $id)->update([
            'order_id' => $request->order_id,
            'product_id' => $request->product_id,
            'qty' => $request->qty,
        ]);
        $orderitem = OrderItem::where('id', $id)->get();
        if($result){
            return response()->json(["result" => "Data berhasil diubah!", 'data' => $orderitem]);
        }
        else
        {
            return response()->json(["result" => "Ubah data gagal!"], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($order_id, $id)
    {
        OrderItem::where('order_id', $order_id)->where('id', $id)->delete();
        $orderitem = OrderItem::where('order_id', $order_id)->get();
        return response()->json(['data' => $orderitem]);
    }
}
